Our award-winning business centre in Reading Berkshire features a large range of office sizes (1-100 desks), stocked kitchens and a fully equipped gym exclusively for clients. Just a 5 minute walk from the train station.

Address: Soane Point, 6-8 Market Place, Reading, Berkshire RG1 2EG, UK

Phone: +44 800 917 4444

Website: https://www.beoffices.com/servicedoffice-locations/south-east/reading
